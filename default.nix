{ fs
, fs2source
, inputs
, lib
, lu-pkgs
, onlyExts
, onlyExts-fs
, packages
, pkgs
, system
, ...
}:
let
  hlib = pkgs.haskell.lib;

  onlyHaskell = fs2source
    (fs.union
      (onlyExts-fs
        [
          "cabal"
          "hs"
          "project"
        ]
        ./.)
      ./LICENSE # horizon requires this file to build
    );

  myOverlay = final: _prev: {
    redshift = final.callCabal2nix "redshift" (onlyHaskell ./.) { };
    regex-pcre = final.callHackage "regex-pcre" "0.95.0.0" { };
    aeson-generics-typescript =
      inputs.aeson-generics-typescript.packages.${system}.default;
    servant-aeson-generics-typescript =
      inputs.servant-aeson-generics-typescript.packages.${system}.default;
    webdriver = final.callCabal2nix "webdriver"
      (builtins.fetchGit {
        url = "https://github.com/haskell-webdriver/haskell-webdriver.git";
        ref = "main";
        rev = "b38d220375989cfe45968ba3abbd24e9f8edf9fc";
      })
      { };
    directory-tree = final.callHackage "directory-tree" "0.12.1" { };
  };

  legacyPackages =
    inputs.horizon-platform.legacyPackages.${system}.extend
      myOverlay;

  hlintqq = pkgs.writeShellScriptBin "hlint" ''
    ${lib.getExe pkgs.hlint} -XQuasiQuotes "$@"
  '';

  dbs = {
    migration = "migration_db";
    endpoint = "endpoint_db";
    websocket = "websocket_db";
  };
in
{
  inherit legacyPackages;

  shell-inputs = with pkgs; [
    cabal-install
    hlintqq
    (import inputs.local-postgres { inherit pkgs; })
    lu-pkgs.cabal-fmt
    lu-pkgs.stylish-haskell
    postgresql
    zlib
  ]
  ++ lib.optionals (system == "x86_64-linux")
    (with pkgs; [
      chromium
      chromedriver
      selenium-server-standalone
      legacyPackages.ghcid
      legacyPackages.haskell-language-server
    ]);

  packages =
    let
      oneTarget = target:
        let
          package =
            # Do not use profiling or add documentation. These are not only
            # wasteful, but cause the build to fail, by triggering a second compilation.
            hlib.justStaticExecutables
              # Don't configure the project to run tests, we are building only one binary here.
              # Tests will fail as well as some suites are integration tests with environmental
              # requirements we provide in the Nix VM Tests.
              (hlib.dontCheck
                # Set the build target to the exe alone
                (hlib.setBuildTarget legacyPackages.redshift target));
        in
        if lib.hasPrefix "exe:" target then
          package.overrideAttrs
            { meta.mainProgram = lib.removePrefix "exe:" target; }
        else
          package;

      client-generator = oneTarget "exe:redshift-client-generator";
    in
    {
      server = oneTarget "exe:redshift-server";

      client =
        pkgs.runCommand "client.ts" { buildInputs = [ client-generator ]; }
          "redshift-client-generator $out";
    };

  checks = lu: {
    cabal-formatting = lu.cabal-fmt { src = onlyExts [ "cabal" ] ./.; };

    haskell-warnings = lu.werror { pkg = legacyPackages.redshift; };

    haskell-formatting = lu.stylish-haskell {
      src =
        fs2source
          (fs.unions [
            (onlyExts-fs [ "hs" ] ./.)
            ./.stylish-haskell.yaml
            ./redshift.cabal
          ])
          ./.;
    };

    haskell-linting = lu.hlint.override { hlint = hlintqq; } {
      src = fs2source (fs.union (onlyExts-fs [ "hs" ] ./.) ./.hlint.yaml) ./.;
    };

    client =
      pkgs.runCommand "client-check" { buildInputs = [ pkgs.typescript ]; } ''
        tsc --lib "ES2021","DOM" ${packages.client} --noEmit --strict
        echo 0 > $out
      '';

    # run interactively using `nix run .#checks.x86_64-linux.vm-tests.driverInteractive`
    vm-tests =
      let nixos-lib = import (inputs.nixpkgs + /nixos/lib) { }; in
      nixos-lib.runTest
        (import ./vm-tests.nix {
          inherit dbs;
          pkgs = pkgs // {
            redshift = packages.server;
            redshift-tests = hlib.justStaticExecutables
              (hlib.dontCheck
                (hlib.setBuildTarget legacyPackages.redshift "exe:redshift-tests"));
          };
        });
  };

  shell-funcs.data."Database Functions" =
    let
      pg-dir = "server/.database";
      constr-var = "DB_CONSTR";

      check-for-db = body: ''
        if [[ -e ${pg-dir} ]]; then
          ${body}
        else
          echo 'No database exists, run `reset-db` and try again.'
        fi
      '';

    in
    {
      create-db = {
        description = "create the local database and add a users table";
        internal = true;
        body =
          let
            init = pkgs.writeText "init.sql" ''
              CREATE DATABASE ${dbs.migration} TEMPLATE = template0;
              CREATE DATABASE ${dbs.endpoint} TEMPLATE = template0;
              CREATE DATABASE ${dbs.websocket} TEMPLATE = template0;
            '';
          in
          ''
            if [[ -e ${pg-dir} ]]; then
              echo database has already been created
            else
              lpg make ${pg-dir}
              lpg pg-up ${pg-dir}
              lpg cmd ${pg-dir} psql -f ${init}
              lpg pg-down ${pg-dir}
            fi
          '';
      };

      test-db-server-start = {
        description = "start the test database";
        body = check-for-db ''
          reset-db
          lpg pg-up ${pg-dir}
          # export ${constr-var}=$(lpg cmd ${pg-dir} bash -c 'echo $LPG_CONNSTR')

          export LPG_LOC=$(lpg cmd ${pg-dir} bash -c 'echo $LPG_LOC')

          export ${lib.toUpper dbs.migration}="postgresql://postgres@localhost/${dbs.migration}?host=$LPG_LOC/socket"
          export ${lib.toUpper dbs.endpoint}="postgresql://postgres@localhost/${dbs.endpoint}?host=$LPG_LOC/socket"
          export ${lib.toUpper dbs.websocket}="postgresql://postgres@localhost/${dbs.websocket}?host=$LPG_LOC/socket"
        '';
      };

      db-server-start = {
        description = "start the database server";
        body = check-for-db ''
          lpg pg-up ${pg-dir}
          export ${constr-var}=$(lpg cmd ${pg-dir} bash -c 'echo $LPG_CONNSTR')
        '';
      };

      db-server-stop = {
        description = "stop the database server";
        body = ''
          lpg pg-down ${pg-dir}
          unset ${constr-var}
          unset ${lib.toUpper dbs.migration} ${lib.toUpper dbs.endpoint} ${lib.toUpper dbs.websocket}
        '';
      };

      db-shell = {
        description = "enter a psql shell for the local database";
        body = check-for-db ''
          db-server-start
          lpg cmd ${pg-dir} psql
        '';
      };

      reset-db = {
        description = "destroy (if it exists) and then recreate the local database";
        body = ''
          destroy-db
          create-db
        '';
      };

      destroy-db = {
        description = "destroy the local database";
        body = ''
          db-server-stop
          rm ${pg-dir} -r
        '';
      };
    };
}
