{
  nixConfig = {
    extra-substituters = "https://horizon.cachix.org";
    extra-trusted-public-keys = "horizon.cachix.org-1:MeEEDRhRZTgv/FFGCv3479/dmJDfJ82G6kfUDxMSAw0=";
  };

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    horizon-platform.url = "git+https://gitlab.horizon-haskell.net/package-sets/horizon-platform";
    lint-utils = {
      url = "git+https://gitlab.nixica.dev/nix/lint-utils.git";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:Fresheyeball/nixpkgs/env-vars-in-buildNpmPackage";
  };

  outputs =
    inputs@{ lint-utils, nixpkgs, ... }:
      with builtins;
      inputs.flake-utils.lib.eachSystem [
        "x86_64-linux"
        "x86_64-darwin"
        "aarch64-darwin"
      ]
        (system:
        let
          env = rec {
            inherit inputs system;
            packages = inputs.self.packages.${system};

            pkgs = import nixpkgs { inherit system; };
            inherit (pkgs) lib;

            hlib = pkgs.haskell.lib;

            onlyHaskell = fs2source
              (fs.union
                (onlyExts-fs
                  [
                    "cabal"
                    "hs"
                    "project"
                  ]
                  ./.)
                ./LICENSE # horizon requires this file to build
              );

            myOverlay = final: _prev: {
              sasha = final.callCabal2nix "sasha" (onlyHaskell ./.) { };
            };

            legacyPackages =
              inputs.horizon-platform.legacyPackages.${system}.extend
                myOverlay;

            fs = lib.fileset;
            fs2source =
              fs': path:
              fs.toSource {
                root = path;
                fileset = fs';
              };
            onlyExts-fs = exts: fs.fileFilter (f: foldl' lib.or false (map f.hasExt exts));
            onlyExts = exts: path: fs2source (onlyExts-fs exts path) path;
            lu-pkgs = lint-utils.packages.${system};
          };
        in
        with env;
        {
          devShells.default = legacyPackages.sasha.env.overrideAttrs
            (attrs: {
              buildInputs =
                attrs.buildInputs
                ++ (with pkgs; [
                  lu-pkgs.nixpkgs-fmt
                  cabal-install
                  lu-pkgs.cabal-fmt
                  lu-pkgs.hlint
                  lu-pkgs.stylish-haskell
                  ghcid
                  haskell-language-server
                ]);
            });


          packages =
            let
              oneTarget = target:
                let
                  package =
                    # Do not use profiling or add documentation. These are not only
                    # wasteful, but cause the build to fail, by triggering a second compilation.
                    hlib.justStaticExecutables
                      # Don't configure the project to run tests, we are building only one binary here.
                      # Tests will fail as well as some suites are integration tests with environmental
                      # requirements we provide in the Nix VM Tests.
                      (hlib.dontCheck
                        # Set the build target to the exe alone
                        (hlib.setBuildTarget legacyPackages.sasha target));
                in
                if lib.hasPrefix "exe:" target then
                  package.overrideAttrs
                    { meta.mainProgram = lib.removePrefix "exe:" target; }
                else
                  package;
            in
            { };

          checks =
            let
              lu = lint-utils.linters.${system};
              nixOnly = onlyExts [ "nix" ] ./.;
            in
            {
              nix-formatting = lu.nixpkgs-fmt { src = nixOnly; };
              nix-dce = lu.deadnix { src = nixOnly; };
              nix-linting = lu.statix { src = nixOnly; };
              cabal-formatting = lu.cabal-fmt { src = onlyExts [ "cabal" ] ./.; };

              haskell-warnings = lu.werror { pkg = legacyPackages.sasha; };

              haskell-formatting = lu.stylish-haskell {
                src =
                  fs2source
                    (fs.unions [
                      (onlyExts-fs [ "hs" ] ./.)
                      ./.stylish-haskell.yaml
                      ./sasha.cabal
                    ])
                    ./.;
              };

              haskell-linting = lu.hlint {
                src = fs2source (fs.union (onlyExts-fs [ "hs" ] ./.) ./.hlint.yaml) ./.;
              };
            };

          formatter = lu-pkgs.nixpkgs-fmt;
        });
}
